#include "testutil.hpp"
#include "settings.hpp"

int main(void)
{
        void *ctx = zmq_ctx_new ();
        assert (ctx);

        unsigned short msg_body = ZMQ_MSG;
        const char* msg_group = ZMQ_TOPIC;
        zmq_msg_t msg;
        zmq_msg_t *msg_ = &msg;

        void *radio = zmq_socket (ctx, ZMQ_RADIO);

        /* Note: For elmo, we just open one connection with GROUP IP (127.0.0.1) and PORT (60000).
           The network will route packets to their destination (as configured by the controller).
           And, virtual switches will update the desintation ports (i.e., to 60000, 60001 ...) for
           subscribers (i.e., dish apps) running on their particular servers. */
        int rc = zmq_connect (radio, "udp://127.0.0.1:60000");
        assert (rc == 0);

        msleep (SETTLE_TIME);

        while (true)
        {
                rc = zmq_msg_init_size (msg_, sizeof (unsigned short));
                assert (rc == 0);
                *((unsigned short *)zmq_msg_data (msg_)) = msg_body;
                rc = zmq_msg_set_group (msg_, msg_group);
                assert (rc == 0);
                rc = zmq_msg_send (msg_, radio, 0);
                assert (rc != -1);
                zmq_msg_close (msg_);
                msleep (1);
        }

        rc = zmq_close (radio);
        assert (rc == 0);

        rc = zmq_ctx_term (ctx);
        assert (rc == 0);
        return 0;
}
