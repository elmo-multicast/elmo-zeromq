# Elmo ZeroMQ

### Install prerequisites
```
make install-prereqs  # optional, if already installed
make setup-libzmq  # Install ZeroMQ library
```

### Build pub/sub (radio/dish) apps for both unicast and elmo
```
make 
```

This will generate dish_0,1,2 and radio_unicast and radio_elmo
binaries under the ./build folder.

##### For unicast:

Start radio in one shell:
```
./build/radio_unicast
```

In three separate shells, run the dish_0,1,2:
```
./build/dish_[0,1,2]
```

You should start seeing `zeromq: abcd` output on the dish
terminals.


##### For elmo:
Start radio in one shell:
```
./build/radio_unicast
```

In a separate shell, run the dish_0:
```
./build/dish_0
```

You should start seeing `zeromq: abcd` output on the dish
terminal.

If you have configured elmo in the network properly, then you
can add more dish apps without modifying the `radio_elmo` app.

> Comment from `radio_elmo` [Line 16]:
>
> `Note: For elmo, we just open one connection with GROUP IP (127.0.0.1) and PORT (60000). 
> The network will route packets to their destination (as configured by the controller). 
> And, virtual switches will update the desintation ports (i.e., to 60000, 60001 ...) for
> subscribers (i.e., dish apps) running on their particular servers.`