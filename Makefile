LIBZMQ_DIR = ../libzmq

all:
	mkdir build
	cp settings.hpp $(LIBZMQ_DIR)/tests

	# Build subscribers (i.e., dish apps)
	cp dish_*.cpp $(LIBZMQ_DIR)/tests
	cd $(LIBZMQ_DIR)/tests && \
		gcc dish_0.cpp -o dish_0 -lzmq -O2 && \
		gcc dish_1.cpp -o dish_1 -lzmq -O2 && \
		gcc dish_2.cpp -o dish_2 -lzmq -O2
	cp $(LIBZMQ_DIR)/tests/dish_? build

	# Build unicast publisher (i.e., radio app)
	cp radio_unicast.cpp $(LIBZMQ_DIR)/tests
	cd $(LIBZMQ_DIR)/tests && \
		gcc radio_unicast.cpp -o radio_unicast -lzmq -O2
	cp $(LIBZMQ_DIR)/tests/radio_unicast build

	# Build elmo publisher (i.e., radio app)
	cp radio_elmo.cpp $(LIBZMQ_DIR)/tests
	cd $(LIBZMQ_DIR)/tests && \
		gcc radio_elmo.cpp -o radio_elmo -lzmq -O2
	cp $(LIBZMQ_DIR)/tests/radio_elmo build

clean:
	-rm -f $(LIBZMQ_DIR)/tests/settings.hpp
	-rm -f $(LIBZMQ_DIR)/tests/dish_*
	-rm -f $(LIBZMQ_DIR)/tests/radio_*
	-rm -rf build

# Prerequisites and configurations

install-prereqs:
	sudo apt-get install build-essential libtool autoconf cmake pkg-config

setup-libzmq:
	git clone git://github.com/zeromq/libzmq.git $(LIBZMQ_DIR)
	cd $(LIBZMQ_DIR) && \
		git checkout v4.2.0 && \
		./autogen.sh && \
		./configure --enable-drafts && \
		make -j4 && \
		sudo make install && \
		sudo ldconfig

clean-libzmq:
	-cd $(LIBZMQ_DIR) && sudo make uninstall && sudo ldconfig
	-rm -rf $(LIBZMQ_DIR)
