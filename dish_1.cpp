#include "testutil.hpp"
#include "settings.hpp"

int main (void)
{
        void *ctx = zmq_ctx_new();
        assert (ctx);

        zmq_msg_t msg;
        zmq_msg_t *msg_ = &msg;

        void *dish = zmq_socket (ctx, ZMQ_DISH);

        int rc = zmq_bind (dish, "udp://*:60001" );
        assert (rc == 0);

        msleep (SETTLE_TIME);

        rc = zmq_join (dish, ZMQ_TOPIC);
        assert (rc == 0);

        while (true)
        {
                rc = zmq_msg_init (msg_);
                assert (rc == 0);
                rc = zmq_msg_recv (msg_, dish, 0);
                printf ("%s: ", zmq_msg_group (msg_));
                unsigned short msg_body = *((unsigned short *)zmq_msg_data (msg_));
                printf("%x\n", msg_body);
                zmq_msg_close (msg_);
        }

        rc = zmq_close (dish);
        assert (rc == 0);

        rc = zmq_ctx_term (ctx);
        assert (rc == 0);

        return 0;
}
